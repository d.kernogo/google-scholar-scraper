import random
import time

import undetected_chromedriver as uc
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from sqlalchemy import create_engine

from sqlalchemy.orm import DeclarativeBase, Session
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column


def random_sleep(secs):
    time.sleep(secs * random.uniform(0.8, 1.2))

class Base(DeclarativeBase):
    pass


class Paper(Base):
    __tablename__ = "paper"

    id: Mapped[int] = mapped_column(primary_key=True)
    url: Mapped[str]
    title: Mapped[str]
    abstract: Mapped[str]
    text: Mapped[str]


engine = create_engine("postgresql+psycopg2://postgres:123456@localhost:5432/postgres", echo=False)
Base.metadata.create_all(engine)

chrome_options = Options()
chrome_options.add_argument('--disable-popup-blocking')
driver = uc.Chrome(headless=False, use_subprocess=False, options=chrome_options)


def process_sciencedirect(partial_abstract) -> tuple[str, str]:
    abstract = driver.find_element(By.XPATH, '//*[@class="abstract author"]').text

    driver.execute_script('''
    let navigation = document.getElementById("issue-navigation")
    if (navigation) {
        navigation.remove()
    }
    let article = document.getElementsByTagName("article")[0]
    while (article.lastChild.className !== 'Tail text-s') {
        console.log(article.lastChild.id)
        article.lastChild.remove()
    }
    ''')

    text = driver.find_element(By.XPATH, '//article').text

    return abstract, text


def process_mdpi(partial_abstract) -> tuple[str, str]:
    abstract = driver.find_element(By.XPATH, '//*[@id="html-abstract"]/div[@class="html-p"]').text

    driver.execute_script('''
    let toRemove = document.getElementsByClassName("NLM_back")
    if (toRemove) {
        Array.from(toRemove).forEach(el => el.remove())
    }
    toRemove = document.getElementsByClassName("articleCitedByDropzone")
    if (toRemove) {
        Array.from(toRemove).forEach(el => el.remove())
    }
    toRemove = document.getElementsByClassName("pdf-download__outer-wrapper")
    if (toRemove) {
        Array.from(toRemove).forEach(el => el.remove())
    }
    toRemove = document.getElementsByClassName("html-back")
    if (toRemove) {
        Array.from(toRemove).forEach(el => el.remove())
    }
    ''')

    text = driver.find_element(By.XPATH, '//article').text

    return abstract, text


def process_linkspringler(partial_abstract) -> tuple[str, str]:
    abstract = driver.find_element(By.XPATH, '//*[@id="Abs1-content"]/p').text

    driver.execute_script('''
        let toRemove = document.querySelectorAll('[data-title="References"]')
        if (toRemove) {
            Array.from(toRemove).forEach(el => el.remove())
        }
        ''')

    text = driver.find_element(By.XPATH, '//article/div/main').text

    return abstract, text


def process_linkspringler(partial_abstract) -> tuple[str, str]:
    abstract = driver.find_element(By.XPATH, '//*[@id="Abs1-content"]/p').text

    driver.execute_script('''
        let toRemove = document.querySelectorAll('[data-title="References"]')
        if (toRemove) {
            Array.from(toRemove).forEach(el => el.remove())
        }
        ''')

    text = driver.find_element(By.XPATH, '//article/div/main').text

    return abstract, text



def process_pubsacs(partial_abstract) -> tuple[str, str]:
    abstract = driver.find_element(By.XPATH, '//*[@id="abstractBox"]/*[@class="articleBody_abstractText"]').text

    text = 'unknown url type!!!'

    return abstract, text


def process_pubsrcs(partial_abstract) -> tuple[str, str]:
    abstract = driver.find_element(By.XPATH, '//*[@class="abstract"]/p').text

    text = 'unknown url type!!!'

    return abstract, text


def process_paper():
    print('processing paper')
    random_sleep(3)
    with (Session(engine) as session):
        partial_abstract = paper.find_element(By.XPATH, value='.//div[@class="gs_rs"]').text.splitlines()[0]
        paper_link_element = paper.find_element(By.XPATH, value='.//h3[@class="gs_rt"]/a')
        paper_url = paper_link_element.get_attribute('href')
        print(f'paper_url={paper_url}')
        title = paper_link_element.text

        driver.execute_script("window.open('');")
        random_sleep(0.5)
        driver.switch_to.window(driver.window_handles[1])
        random_sleep(0.5)
        exception_while_getting_article = False
        try:
            driver.get(paper_url)
        except Exception as e:
            print('exception while getting article')
            exception_while_getting_article = True
        random_sleep(0.5)

        try:
            if exception_while_getting_article:
                abstract, text = 'exception', 'exception'
            elif paper_url.__contains__('www.sciencedirect.com'):
                abstract, text = process_sciencedirect(partial_abstract)
            elif paper_url.__contains__('www.mdpi.com'):
                abstract, text = process_mdpi(partial_abstract)
            elif paper_url.__contains__('link.springer.com'):
                abstract, text = process_linkspringler(partial_abstract)
            elif paper_url.__contains__('pubs.acs.org'):
                abstract, text = process_pubsacs(partial_abstract)
            elif paper_url.__contains__('pubs.rsc.org'):
                abstract, text = process_pubsrcs(partial_abstract)
            else:
                print('unknown url type!!!')
                abstract, text = 'unknown url type!!!', 'unknown url type!!!'
        except Exception as e:
            print('Got exception ', e)
            abstract, text = 'exception', 'exception'

        session.add(Paper(url=paper_url, title=title, abstract=abstract, text=text))
        session.commit()

        print(f'title={title}')
        driver.close()
        driver.switch_to.window(driver.window_handles[0])
    random_sleep(1)



driver.get(f'https://scholar.google.com/scholar?as_vis=1&start=0&q=pharmaceutics&hl=en&as_sdt=0,5&as_ylo=2023')
time.sleep(30)
for i in range(23, 1000):
    paper_list_div = driver.find_element(by=By.ID, value='gs_res_ccl_mid')
    paper_list = paper_list_div.find_elements(by=By.XPATH, value='//div[@class="gs_r gs_or gs_scl"]')

    for paper in paper_list:
        process_paper()

    driver.get(f'https://scholar.google.com/scholar?as_vis=1&start={(i + 1) * 10}&q=pharmaceutics&hl=en&as_sdt=0,5&as_ylo=2023')
